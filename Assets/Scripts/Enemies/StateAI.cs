﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAI : MonoBehaviour
{
    public virtual void Move()
    {
        //TODO Déplacement des ennemis avec le NavMesh
    }

    public virtual void PlaceMine()
    {
        //TODO Placement de l'objet mine
    }

    public virtual void Shoot()
    {
        //TODO Tir pas 100% précis
    }

    public virtual void Attack()
    {
        //TODO Attaque CAC
    }

    public virtual void UpdateStats()
    {
        //TODO Augmentation des stats pour la phase 2
    }
}
