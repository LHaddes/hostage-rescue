﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    private CharacterController _characterController;
    private Vector3 moveDir;

    [Header("Jump")] public float jumpForce;
    private Vector3 velocity;
    public float gravity = -9.81f;
    private bool isGrounded;
    public Transform groundCheckPoint;
    public float groundCheckRange;
    public LayerMask groundLayer;

    [Header("Camera")] 
    [Range(70, 200)]
    public float mouseSensitivityX;
    [Range(70, 200)]
    public float mouseSensitivityY;
    private Camera cam;
    private float xRotation;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _characterController = GetComponent<CharacterController>();
        cam = Camera.main;

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        #region Movement

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        moveDir = transform.right * horizontal + transform.forward * vertical;
        
        _characterController.Move(moveDir * movementSpeed * Time.deltaTime);

        #endregion


        #region Jump

        #region GroundCheck
        
        Debug.DrawRay(groundCheckPoint.position, Vector3.down * groundCheckRange, Color.red);

        RaycastHit hit;
        if (Physics.Raycast(groundCheckPoint.position, Vector3.down, out hit, groundCheckRange, groundLayer))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        
        #endregion

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("J");
            velocity.y = Mathf.Sqrt(jumpForce * -2 * gravity);
        }
        if (isGrounded && velocity.y < 0)
        {
            Debug.Log("Reset y velocityy");
            velocity.y = 0f;
        }

        velocity.y += gravity * Time.deltaTime;

        _characterController.Move(velocity * Time.deltaTime);

        #endregion
        

        LookDirection();
    }

    void LookDirection()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivityX * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivityY * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }

    
    public override void Death()
    {
        throw new System.NotImplementedException();
    }
}
