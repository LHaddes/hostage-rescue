﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    [Header("Life")]
    public float currentHealth;
    public float maxHealth;

    [Header("Movement")] 
    public float movementSpeed;
    
    
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void Hurt(int damage)
    {
        currentHealth -= damage;
    }

    public abstract void Death();
}
